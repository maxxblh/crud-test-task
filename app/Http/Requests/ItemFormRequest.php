<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class ItemFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    protected function prepareForValidation()
    {
        if (in_array($this->getMethod(), ['PUT', 'DELETE'])) {
            $this->merge(['id' => $this->route('todo')]);
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return $this->getRuleByMethod($this->getMethod());
    }

    private function getRuleByMethod(string $method): array
    {
        return match ($method) {
            'POST' => [
                'content' => ['required', 'string', 'max:191']
            ],
            'PUT' => [
                'id'      => ['required', 'numeric', 'exists:App\Models\Item,id'],
                'content' => ['required', 'string', 'max:191']
            ],
            'DELETE' => [
                'id' => ['required', 'numeric', 'exists:App\Models\Item,id'],
            ]
        };
    }

    public function messages(): array
    {
        return [
            'id.required'      => 'Missing id',
            'content.required' => 'Please fill up the content field',
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param Validator $validator
     * @return void
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            response()->json(['message' => $validator->errors()->first()], 422)
        );
    }
}
