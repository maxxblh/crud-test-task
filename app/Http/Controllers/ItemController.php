<?php

namespace App\Http\Controllers;

use App\Http\Requests\ItemFormRequest;
use App\Http\Resources\ItemResource;
use App\Repositories\ItemRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;

class ItemController extends BaseController
{
    private ItemRepository $repository;

    public function __construct(ItemRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(): JsonResponse
    {
        try {
            $item_list = $this->repository->all();
        } catch (\Throwable $e) {
            Log::error($e->getMessage());
            return $this->sendError();
        }

        return $this->sendResponse(
            'Items fetched.',
            ['list' => ItemResource::collection($item_list)]
        );
    }

    public function store(ItemFormRequest $request): JsonResponse
    {
        try {
            $content = $request->input('content');
            $item = $this->repository->create($content);
        } catch (\Throwable $e) {
            Log::error($e->getMessage());
            return $this->sendError();
        }

        return $this->sendResponse(
            'Item created.',
            ['id' => $item->id],
            201
        );
    }

    public function update(ItemFormRequest $request): JsonResponse
    {
        try {
            $id = $request->input('id');
            $content = $request->input('content');
            $this->repository->update($id, $content);
        } catch (\Throwable $e) {
            Log::error($e->getMessage());
            return $this->sendError();
        }

        return $this->sendResponse('Item updated.');
    }

    public function destroy(ItemFormRequest $request): JsonResponse
    {
        try {
            $id = $request->input('id');
            $this->repository->destroy($id);
        } catch (\Throwable $e) {
            Log::error($e->getMessage());
            return $this->sendError();
        }

        return $this->sendResponse('Item deleted');
    }
}
