<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;

class BaseController extends Controller
{

    /**
     * @param string $message
     * @param array $data
     * @param int $code
     * @return JsonResponse
     */
    public function sendResponse(string $message, array $data = [], int $code = 200): JsonResponse
    {
        $response = array_merge(['message' => $message], $data);
        return response()->json($response, $code);
    }

    /**
     * @param string $error
     * @param int $code
     * @return JsonResponse
     */
    public function sendError(string $error = '', int $code = 500): JsonResponse
    {
        $response = [
            'message' => $error ?? 'External API call failed.',
        ];
        return response()->json($response, $code);
    }
}
