<?php

namespace App\Repositories;

use App\Models\Item;
use App\Repositories\Interfaces\ItemRepositoryInterface;

class ItemRepository implements ItemRepositoryInterface
{
    public function all()
    {
        return Item::all();
    }

    public function create(string $content)
    {
        return Item::create([
            'content' => $content,
        ]);
    }

    public function update(int $id, string $content)
    {
        return Item::whereId($id)->update([
            'content' => $content
        ]);
    }

    public function destroy(int $id)
    {
        return Item::whereId($id)->delete();
    }
}
