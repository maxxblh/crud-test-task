<?php

namespace App\Repositories\Interfaces;

interface ItemRepositoryInterface
{
    public function all();

    public function create(string $content);

    public function update(int $id, string $content);

    public function destroy(int $id);
}
