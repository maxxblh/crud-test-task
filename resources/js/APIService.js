import axios from "axios";
const request = axios.create({
    withCredentials: true,
    baseURL: "/api/",
});

/**
 * APIService
 */
export class APIService {
    static getTodoList() {
        return request.get("/todo ");
    }

    static createTodoItem(data) {
        return request.post("/todo ", data);
    }

    static updateTodoItem(id, data) {
        return request.put(`/todo/${id}`, data);
    }

    static deleteTodoItem(id) {
        return request.delete(`/todo/${id}`);
    }
}
