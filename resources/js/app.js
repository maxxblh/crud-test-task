import Vue from "vue";
import App from "./App.vue";
import VueAxios from "vue-axios";
import axios from "axios";
import Toasted from 'vue-toasted';
import { option } from "./toasted-options";

require("./bootstrap");

window.Vue = require("vue");
Vue.use(VueAxios, axios);
Vue.use(Toasted, option)

const app = new Vue({
    el: "#app",
    components: { App },
    template: "<App/>",
});
