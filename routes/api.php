<?php

use Illuminate\Support\Facades\Route;

Route::middleware('api')->group(function () {
    Route::resource('todo', App\Http\Controllers\ItemController::class)
        ->only(['index', 'store', 'update', 'destroy']);
});
